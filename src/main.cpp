/*
** main.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:13:06 2015 Alexis Bertholom
// Last update Wed Jan  3 02:22:16 2018 Lucas
*/

#include <unistd.h>
#include <iostream>
#include "SDLDisplay.hpp"
#include "Input.hpp"
#include "Colors.hpp"
#include "RNG.hpp"
bool RNG::_initialized = false;

int		main()
{
  SDLDisplay	display("minijeu", 800, 800);
  Input		input;
  // Les déclarations de variables se font ici

  
  while (!(input.shouldExit()) && !(input.getKeyState(SDL_SCANCODE_ESCAPE)))
    {
      display.clearScreen(); // "Nettoie" la fenêtre

      // Le reste de votre code ici
      display.putRect(300, 200, 100, 50, Colors::Violet); // Créé un carré violet sur la fenêtre, vous pouvez
                                                          // enlever cette ligne, elle est là pour l'exemple :)
      
      display.refreshScreen(); // Rafraîchit la fenêtre 
      input.flushEvents();
    }
  return (0);
}
